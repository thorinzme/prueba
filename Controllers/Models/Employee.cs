using System;
using Models;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
namespace Controllers.Models
{
    public class _Employee
    {
        public string Name{get; set;}
        public string LastName{ get; set;}
        private string _RFC { get; set;}
        public DateTime BornDate { get; set; }
        public EmployeeStatus Status { get; set; }

        public string RFC{
        get{ 
            return this._RFC;
        }
        set{

                var regex = new Regex(@"^\D{3,4}\d{4,4}\w{5,5}$");

                if( !(regex.IsMatch(value))){
                    throw new Exception("not is a RFC");
                }
                else{
                    this._RFC = value;

                }
            }
        }
    }
 

}