using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Models;
using Microsoft.Extensions.Configuration;
using System.Data;
using Npgsql;
using Controllers.Models;

namespace Test.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EmployeeController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<EmployeeController> _logger;

        private readonly EmployeeContext _context;


        public EmployeeController( IConfiguration configuration,  ILogger<EmployeeController> logger, EmployeeContext context)
        {
            _logger = logger;
            _configuration = configuration;
            _context = context;
        }

        


        [HttpPost]
        public JsonResult Post(_Employee empleado)
        {
            Employee new_empleado = new Employee{
                RFC = empleado.RFC,
                BornDate = empleado.BornDate,
                LastName = empleado.LastName,
                Status = empleado.Status,
                Name = empleado.Name    

            };
            
            try
            {
                _context.Empleados.Add(new_empleado);
                _context.SaveChanges();
                return new JsonResult(string.Format("Se creo el registro con RFC {0}", empleado.RFC)) ;
            }
            
            catch (Exception ex)
            {
                return new JsonResult(ex.InnerException.Message );
            }


        }

        [HttpGet]
        public JsonResult Get([FromQuery] string nombre)
        {
            var listOnBase = new List<Employee>();
            if (nombre == null)
                listOnBase = _context.Empleados.OrderBy(x => x.BornDate).ToList();
            else
                listOnBase = _context.Empleados.OrderBy(x => x.BornDate).Where(x => x.Name == nombre).ToList();
            
            
            return new JsonResult(listOnBase);

        }

    }
}
