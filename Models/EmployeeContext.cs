using Microsoft.EntityFrameworkCore;
using System.Data;

namespace Models
{
    public class EmployeeContext: DbContext
    {
        public EmployeeContext( DbContextOptions<EmployeeContext> options ): base(options ){}
        public DbSet<Employee> Empleados { get; set;}

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Employee>()
                .HasIndex(u => u.RFC)
                .IsUnique();
        }
    }
}

